version: '3'

services:
  frontend:
    build:
      context: .
    volumes:
      - '.:/app'
      - '/app/node_modules'
    ports:
      - '$$external_frontend_port$$:$$internal_frontend_port$$'
