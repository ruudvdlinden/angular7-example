FROM node:12.2.0

# Install chrome for protractor tests
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
RUN apt-get update && apt-get install -yq google-chrome-stable

# Set working directory
WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

# Install and cache app dependencies
COPY package.json /app/package.json
RUN npm cache clean --force
RUN npm install --no-cache
RUN npm install -g --no-cache $$angular_cli$$  # To keep sync with package.json

# add app
COPY . /app

# start app
CMD ng serve --host 0.0.0.0 --poll
