import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  languages = ['en', 'nl'];

  constructor(public translate: TranslateService) {
    // Sets app language from browser language if supported, otherwise uses a default

    const default_language = this.languages[0];
    const browserLang = translate.getBrowserLang();

    translate.addLangs(this.languages);
    translate.setDefaultLang(default_language);

    const regexFromLanguages = new RegExp(`^${ this.languages.join('|') }$`, 'i');
    const language = browserLang.match(regexFromLanguages) ? browserLang : default_language;

    translate.use(language);
  }
}
