import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { RestService } from './rest.service';

describe('RestService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    declarations: [],
    imports: [ HttpClientTestingModule ]
  }));

  it('should be created', () => {
    // Tests if the REST service itself can be build
    const service: RestService = TestBed.get(RestService);
    expect(service).toBeTruthy();
  });
});
