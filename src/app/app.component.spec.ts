import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { AppComponent } from './app.component';
import { HttpLoaderFactory } from './app.module';

describe('AppComponent', () => {
  let translate: TranslateService;
  let http: HttpTestingController;
  let fixture: ComponentFixture<AppComponent>;
  const TRANSLATIONS_EN: { 'HOME': {'SELECT': object } } = require('../assets/i18n/en.json');
  const TRANSLATIONS_NL: { 'HOME': {'SELECT': object } } = require('../assets/i18n/nl.json');

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
          }
        })
      ],
      declarations: [
        AppComponent
      ],
      providers: [TranslateService]
    }).compileComponents();
    translate = TestBed.get(TranslateService);
    http = TestBed.get(HttpTestingController);
    fixture = TestBed.createComponent(AppComponent);
  }));

  it('should create the app', () => {
    // Tests minimal building of the app

    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'app'`, () => {
    // Assert the component correctly gets its properties

    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app');
  });

  it('should render title in a h1 tag', () => {
    // Tests property insertion in the header

    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('app');
  });

  it('should load translations', async(() => {
    // Tests minimal integration of translations in the app

    const compiled = fixture.debugElement.nativeElement;

    spyOn(translate, 'getBrowserLang').and.returnValue('en');  // Mock browser language setting

    http.expectOne('/assets/i18n/en.json').flush(TRANSLATIONS_EN);  // Reroute to correct translation file path
    http.expectNone('/assets/i18n/nl.json');

    fixture.detectChanges();
    // The content should be translated to english now
    expect(compiled.querySelector('#translate_test').textContent).toEqual(TRANSLATIONS_EN.HOME.SELECT);

    translate.use('nl');
    http.expectOne('/assets/i18n/nl.json').flush(TRANSLATIONS_NL);  // Reroute to correct translation file path
    http.expectNone('/assets/i18n/en.json');

    fixture.detectChanges();
    // The content should be translated to dutch now
    expect(compiled.querySelector('#translate_test').textContent).toEqual(TRANSLATIONS_NL.HOME.SELECT);
  }));

  it('should load a default translation when an known browser language is set', async(() => {
    // Checks that there's a default language set for translations, because when no language is set, translation tags
    // are displayed instead.

    const app = fixture.debugElement.componentInstance;
    const compiled = fixture.debugElement.nativeElement;

    const unsupported_language = 'fr';

    expect(app.languages).not.toContain(unsupported_language);
    expect(app.languages[0]).toEqual('en');  // Default language

    spyOn(translate, 'getBrowserLang').and.returnValue(unsupported_language);  // Mock browser language setting

    http.expectOne('/assets/i18n/en.json').flush(TRANSLATIONS_EN);  // Reroute to correct translation file path
    http.expectNone('/assets/i18n/nl.json');

    fixture.detectChanges();
    // The content should be translated to english now
    expect(compiled.querySelector('#translate_test').textContent).toEqual(TRANSLATIONS_EN.HOME.SELECT);
  }));
});
