import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'todo-detail',
  templateUrl: './todo-detail.component.html',
  styleUrls: ['./todo-detail.component.scss']
})

export class TodoDetailComponent {
  date_time_format = 'dd-MM-yyyy HH:mm';
  date_format = 'dd-MM-yyyy';
  time_format = 'HH:mm';

  @Input() todo = { name: '', datetime_start: '' };
  @Input() showDate = true;
}
