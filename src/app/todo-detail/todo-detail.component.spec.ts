import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { TodoDetailComponent } from './todo-detail.component';

describe('TodoDetailComponent', () => {
  let component: TodoDetailComponent;
  let fixture: ComponentFixture<TodoDetailComponent>;
  const test_date = new Date(2019, 3, 7, 2, 4);
  const date_string = test_date.toLocaleString('nl-NL', { year: 'numeric', month: '2-digit', day: '2-digit' });
  const time_string = test_date.toLocaleString('nl-NL', { hour12: false, hour: '2-digit', minute: '2-digit' });
  const test_todo = {name: 'testname', datetime_start: test_date.toISOString()};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodoDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoDetailComponent);
    component = fixture.componentInstance;
    component.todo = test_todo;
    fixture.detectChanges();
  });

  it('should create', () => {
    // Asserts the todo-detail comoonent is able to build. Also checks default value of the showDate property

    expect(component).toBeTruthy();
    expect(component.showDate).toBe(true);
  });

  it('should show the name of the todo', () => {
    // Asserts the name of the todo is correctly inserted.
    const content = fixture.nativeElement.innerText;

    expect(content).toContain(test_todo.name);
  });

  it('should only show the start time of the todo when the showDate property is set to false', () => {
    // When showDate is set to False, only show the time of the todo
    component.showDate = false;
    fixture.detectChanges();
    const content = fixture.nativeElement.innerText;

    expect(content).toContain(time_string);
    expect(content).not.toContain(date_string);
  });

  it('should show the start time and date of the todo when the showDate property is set to true', () => {
    // When showDate is set to True, show both the date and the time of the todo
    component.showDate = true;
    fixture.detectChanges();
    const content = fixture.nativeElement.innerText;

    expect(content).toContain(time_string);
    expect(content).toContain(date_string);
  });
});
