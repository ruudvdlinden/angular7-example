import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

const endpoint = 'http://127.0.0.1:8000/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', })
};

@Injectable({
  providedIn: 'root'
})
export class RestService {
  constructor(private http: HttpClient) {}

  getTodos(): Observable<any> {
    return this.http.get(endpoint + 'todos/');
  }

  addTodo(new_todo): Observable<any> {
    return this.http.post(endpoint + 'todos/', new_todo, httpOptions);
  }

  changeDate(new_data): Observable<any> {
    return this.http.put(endpoint + 'todos/', new_data, httpOptions);
  }
}
