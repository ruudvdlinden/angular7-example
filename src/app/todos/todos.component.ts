import { AfterViewInit, Component, Input, OnInit, QueryList, ViewChildren  } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import * as lodash from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'todos-component',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss']
})

export class TodosComponent implements OnInit {
  todos: object = {};
  other: object[] = [];
  data: object[] = [];
  days_in_strip = 7;
  errors: object = null;

  @Input() todoData = this.getInitialTodoData();

  constructor(public rest: RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.getTodos();
  }

  getInitialTodoData() {
    return { name: '', datetime_start: ''};
  }

  addtodo() {
    const new_todo = this.todoData;
    this.rest.addTodo(new_todo).subscribe((result_todo: {'datetime_start': moment.Moment}) => {
      result_todo.datetime_start = moment(result_todo.datetime_start);  // Convert ISO string to moment object
      this.data.push(result_todo);
      this.orderTodos();
      this.sortTodos();

      this.todoData = this.getInitialTodoData();  // Clear input fields
      this.errors = null;
    }, (result) => {
      this.errors = result.error;
     });
  }

  getTimestampOfDay(date) {
    return moment(date).startOf('day').format('x');
  }

  orderTodos() {
    // Order todos into categories; for the first amount (configured in class constant) of days

    const other = [];
    // Initiate empty array for each day
    const ordered_todos = Array.from(Array(this.days_in_strip).keys()).reduce((obj, day_counter) => {
      obj[this.getTimestampOfDay(moment(new Date()).add(day_counter, 'days'))] = [];
      return obj;
    }, {});

    // Add todos to the correct array
    this.data.map((todo: {'datetime_start': moment.Moment}) => {
      const timestamp = this.getTimestampOfDay(todo.datetime_start);

      if (timestamp in ordered_todos) { ordered_todos[timestamp].push(todo); }
      else { other.push(todo); }
    });

    this.todos = ordered_todos;
    this.other = other;
  }

  getTodos() {
    this.rest.getTodos().subscribe((todos: []) => {
      todos.map((todo: {'datetime_start': string | moment.Moment}) => {
        todo.datetime_start = moment(todo.datetime_start);  // Convert ISO string to moment object
      });

      this.data = todos;
      this.orderTodos();
      this.sortTodos();
    });
  }

  sortTodos() {
    // Sorts the todos within their categories on date and time

    this.other = lodash.orderBy(this.other, ['datetime_start'], ['asc']);

    for (const key of Object.keys(this.todos)) {
      this.todos[key] = lodash.orderBy(this.todos[key], ['datetime_start'], ['asc']);
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    // Changes the datetime_start of the dropped todo, and saves that change in the database
    // If the API call returns a success, the element is moved to the new column

    if (event.previousContainer !== event.container) {
      const todo = event.item.data.todo;
      const destination_day = moment(parseInt(event.container.data['key']));

      // Build new datetime from date of new day, and time of item itself
      const new_datetime_start = destination_day.hour(todo.datetime_start.hour()).minute(todo.datetime_start.minute());

      const new_data = {
        pk: todo.id,
        datetime_start: new_datetime_start.format()
      };

      // Change relevant objects in on success
      this.rest.changeDate(new_data).subscribe((result_todo: {'datetime_start': moment.Moment}) => {
        todo.datetime_start = new_datetime_start;
        transferArrayItem(event.previousContainer.data['data'], event.container.data['data'], event.previousIndex, event.currentIndex);
        this.sortTodos();
      });
    }
  }
}
