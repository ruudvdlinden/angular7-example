import argparse
from pathlib import Path
import re
from typing import Dict


CONFIG: Dict = {
    'general': {
        'internal_frontend_port': 4200,  # Angular default server port
    },
    'develop': {
        'external_frontend_port': 4200,
    },
    'staging': {
        'external_frontend_port': 4201,
    }
}


def parse_args():
    parser = argparse.ArgumentParser(description='Replace variables in docker configuration.')
    parser.add_argument('environment', type=str, help='Environment: can be "develop" or "staging"')

    return parser.parse_args()


if __name__ == '__main__':
    """ Create the Docker compose yaml and the Dockerfile file based on the chosen environment"""

    arguments = parse_args()

    # Fill in environment-specific configuration in composer file
    config = dict(CONFIG['general'])  # Create copy to modify
    config.update(CONFIG[arguments.environment])

    config_text = Path('docker-compose.tpl').read_text()
    for tag, value in config.items():
        config_text = config_text.replace('$$%s$$' % tag, str(value))

    Path('docker-compose.yml').write_text(config_text)

    # Keep the same version in Docker instance as in the app itself
    packages = Path('package.json').read_text()
    angular_cli = re.search(r'@angular/cli": ".*"', packages)[0].split('"')
    docker_file_template = Path('Dockerfile.tpl').read_text()

    Path('Dockerfile', ).write_text(
        docker_file_template.replace('$$angular_cli$$', angular_cli[0] + '@' + angular_cli[2][1:])
    )


